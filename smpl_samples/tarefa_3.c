#include <stdlib.h>
#include <stdio.h>
#include "smpl.h"

typedef enum
{
    TEST,
    FAULT,
    REPAIR,
    PRINT_STATE
} Event;

typedef enum
{
    UNKNOWN,
    FAULTY,
    FAULT_FREE
} NodeState;

static const char *NodeStateName[] = {
    "UNKNOWN",
    "FAULTY",
    "FAULT_FREE"
};

typedef struct
{
    int id;
    NodeState *STATE;
} Node;

Node *nodes;
int numEvents = 0;

void scheduleSimulation(int numNodes);

Node *createSimulation(char *name, int n)
{
    smpl(0, name);
    reset();
    stream(1);

    char fa_name[5];
    Node *nodes = (Node *)malloc(n * sizeof(Node));

    for (int i = 0; i < n; i++)
    {
        memset(fa_name, '\0', 5);
        sprintf(fa_name, "%d", i);
        nodes[i].id = facility(fa_name, 1);
        nodes[i].STATE = (NodeState *)malloc(n * sizeof(NodeState));
        nodes[i].STATE[i] = FAULT_FREE;
    }

    return nodes;
}

void fail(int nodeId, int token)
{
    int r = request(nodeId, token, 0);
    if (r != 0)
    {
        printf("[%4.1f] Não foi possível falhar o nodo %d\n", time(), token);
        fflush(stdout);
        exit(1);
    }
    printf("[%4.1f] Nodo %d falhou\n", time(), token);
    fflush(stdout);
}

void recover(int nodeId, int token, int numNodes)
{
    release(nodeId, token);
    for (int i = 0; i < numNodes; i++)
    {
        nodes[token].STATE[i] = UNKNOWN;
    }
    nodes[token].STATE[token] = FAULT_FREE;
    printf("[%4.1f] Nodo %d recuperado\n", time(), token);
    fflush(stdout);
}

int isFaulty(int nodeId)
{
    return status(nodeId) != 0 ? 1 : 0;
}

int getNextToken(int currentToken, int numNodes)
{
    return (currentToken + 1) % numNodes;
}

int getNextNodeId(int currentToken, int numNodes)
{
    return nodes[getNextToken(currentToken, numNodes)].id;
}

int getNodeId(int token)
{
    return nodes[token].id;
}

int getValidToken(int token, int numNodes)
{
    return (token) % numNodes;
}

void printNodeSTATE(int token, int numNodes)
{
    printf("[STATE] %d: [ %d: %s", token, 0, NodeStateName[nodes[token].STATE[0]]);
    fflush(stdout);
    for (int i = 1; i < numNodes; i++)
    {
        printf(", %d: %s", i, NodeStateName[nodes[token].STATE[i]]);
        fflush(stdout);
    }
    printf(" ]\n");
    fflush(stdout);
}

void scheduleEvent(int evt, real _time, int token)
{
    numEvents++;
    schedule(evt, _time, token);
}

void scheduleIfPossible(int *faultyNodes, int numNodes, int evt, real _time, int token)
{
    if (evt == FAULT && faultyNodes[token] == 0)
    {
        scheduleEvent(evt, _time, token);
        faultyNodes[token] = 1;
    }
    else if (evt == REPAIR && faultyNodes[token] == 1)
    {
        scheduleEvent(evt, _time, token);
        faultyNodes[token] = 0;
    }
    else if (evt == TEST && faultyNodes[token] == 0)
    {
        scheduleEvent(evt, _time, token);
    }
    else if (evt == PRINT_STATE)
    {
        scheduleEvent(evt, _time, token);
    }
}

void scheduleForAll(int *faultyNodes, int evt, int numNodes, real _time)
{
    for (int i = 0; i < numNodes; i++)
    {
        scheduleIfPossible(faultyNodes, numNodes, evt, _time, i);
    }
}

void scheduleSimulation(int numNodes)
{
    /* Recommended number of nodes: 8 */
    int faultyNodes[numNodes];
    memset(faultyNodes, 0, numNodes * sizeof(int));

    scheduleForAll(faultyNodes, TEST, numNodes, 15.0);
    scheduleForAll(faultyNodes, PRINT_STATE, numNodes, 16.0);
    scheduleIfPossible(faultyNodes, numNodes, FAULT, 30.0, getValidToken(4, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, FAULT, 40.0, getValidToken(5, numNodes));
    scheduleForAll(faultyNodes, TEST, numNodes, 45.0);
    scheduleIfPossible(faultyNodes, numNodes, FAULT, 53.0, getValidToken(7, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, FAULT, 55.0, getValidToken(2, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 57.5, getValidToken(4, numNodes));
    scheduleForAll(faultyNodes, TEST, numNodes, 58.0);
    scheduleIfPossible(faultyNodes, numNodes, FAULT, 58.0, getValidToken(1, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 68.0, getValidToken(7, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, FAULT, 130.0, getValidToken(4, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 155.0, getValidToken(4, numNodes));
    scheduleForAll(faultyNodes, TEST, numNodes, 180.0);
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 181.0, getValidToken(2, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 211.0, getValidToken(1, numNodes));
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 255.0, getValidToken(5, numNodes));
    scheduleForAll(faultyNodes, TEST, numNodes, 300.0);
    scheduleForAll(faultyNodes, FAULT, numNodes, 330.0);
    scheduleIfPossible(faultyNodes, numNodes, REPAIR, 331.0, getValidToken(0, numNodes));
    scheduleForAll(faultyNodes, TEST, numNodes, 340.0);
    scheduleForAll(faultyNodes, PRINT_STATE, numNodes, 340.0);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        puts("Uso: ./tarefa_1 <num_nodos>\n\t<num_nodos>: nodos usados na simulação");
        exit(1);
    }

    const int n = atoi(argv[1]);
    int event, currentToken, nextToken, currentId, nextId, nodesTested;

    nodes = createSimulation("Teste em anel simples", n);
    scheduleSimulation(n);

    for (int i = 0; i < numEvents; i++)
    {
        cause(&event, &currentToken);
        nextToken = getNextToken(currentToken, n);
        currentId = getNodeId(currentToken);
        nextId = getNextNodeId(currentToken, n);
        switch (event)
        {
            case TEST:
                if (isFaulty(nextId))
                {
                    nodesTested = 0;
                    do
                    {
                        nodes[currentToken].STATE[nextToken] = FAULTY;
                        printf("[%4.1f] Nodo %d testou %d como falho\n", time(), currentToken, nextToken);
                        fflush(stdout);
                        nextToken = getNextToken(nextToken, n);
                        nextId = getNodeId(nextToken);
                        nodesTested++;
                    }
                    while (isFaulty(nextId) && nodesTested < (n-1));
                    if (nodesTested < (n-1))
                    {
                        nodes[currentToken].STATE[nextToken] = FAULT_FREE;
                        printf("[%4.1f] Nodo %d testou %d como sem-falha\n", time(), currentToken, nextToken);
                        fflush(stdout);
                    }
                    else
                    {
                        printf("[%4.1f] Nodo %d testou todos os nodos do sistema falhos\n", time(), currentToken);
                        fflush(stdout);
                    }
                }
                else
                {
                    nodes[currentToken].STATE[nextToken] = FAULT_FREE;
                    printf("[%4.1f] Nodo %d testou %d como sem-falha\n", time(), currentToken, nextToken);
                    fflush(stdout);
                }
                break;

            case FAULT:
                fail(getNodeId(currentToken), currentToken);
                break;

            case REPAIR:
                recover(getNodeId(currentToken), currentToken, n);
                break;
            
            case PRINT_STATE:
                printNodeSTATE(currentToken, n);
                break;
        }
    }
}