#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define POW_2(num) (1<<(num))
#define VALID_J(j, s) ((POW_2(s-1)) >= j)

/* |-- NodeSet.h */
typedef struct NodeSet {
	int* nodes;
	size_t size;
	size_t offset;
} NodeSet;

static NodeSet*
createNodeSet(size_t size)
{
	NodeSet* new;

	new = (NodeSet*)malloc(sizeof(NodeSet));
	new->nodes = (int*)malloc(sizeof(int)*size);
	new->size = size;
	new->offset = 0;
	return new;
}

static void
NodeSet_insert(NodeSet* nodes, int node)
{
	if (nodes == NULL) return;
	nodes->nodes[nodes->offset++] = node;
}

static void
NodeSet_merge(NodeSet* dest, NodeSet* source)
{
	if (dest == NULL || source == NULL) return;
	memcpy(&(dest->nodes[dest->offset]), source->nodes, sizeof(int)*source->size);
	dest->offset += source->size;
}

static void
deleteNodeSet(NodeSet* nodes)
{
	free(nodes->nodes);
	free(nodes);
}
/* NodeSet.h --| */

NodeSet*
cis(int i, int s)
{
	NodeSet* nodes, *other_nodes;
	int xor = i ^ POW_2(s-1);
	int j;

	/* starting node list */
	nodes = createNodeSet(POW_2(s-1));

	/* inserting the first value (i XOR 2^^(s-1)) */
	NodeSet_insert(nodes, xor);

	/* recursion */
	for (j=1; j<=s-1; j++) {
		other_nodes = cis(xor, j);
		NodeSet_merge(nodes, other_nodes);
		deleteNodeSet(other_nodes);
	}
	return nodes;
}


int
main(int argc, char** argv)
{
	int i, s, j = -1;
	NodeSet* nodes;

	if (argc < 3) {
		printf(" Usage: %s i s [j]\n", argv[0]);
		return 1;
	}

	/* required params */
	i = atoi(argv[1]);
	s = atoi(argv[2]);

	/* have we the third parameter? */
	if (argc > 3) {
		j = atoi(argv[3]);
		if (!VALID_J(j, s)) {
			printf("%d is not a valid node for a cluster size %d.\n", j, s);
			return 1;
		}
	}
	/* calculating cis */
	nodes = cis(i, s);

	/* print all nodes or just one? */
	if (j != -1)
		printf("%i\n", nodes->nodes[j-1]);
	else {
		for (i=0; i < nodes->size; i++)
			printf("%i ", nodes->nodes[i]);
		puts("");
	}
	deleteNodeSet(nodes);
	return 0;
}

