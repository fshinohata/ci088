#include <stdlib.h>
#include <stdio.h>
#include "smpl.h"

typedef enum
{
    TEST,
    FAULT,
    REPAIR
} Event;

typedef enum
{
    UNKNOWN,
    FAULTY,
    FAULT_FREE
} NodeState;

typedef struct
{
    int id;
    NodeState nextNodeState;
} Node;

Node *nodes;
int numEvents = 0;

void scheduleSimulation(int numNodes);

Node *createSimulation(char *name, int n)
{
    smpl(0, name);
    reset();
    stream(1);

    char fa_name[5];
    Node *nodes = (Node *)malloc(n * sizeof(Node));

    for (int i = 0; i < n; i++)
    {
        memset(fa_name, '\0', 5);
        sprintf(fa_name, "%d", i);
        nodes[i].id = facility(fa_name, 1);
        nodes[i].nextNodeState = UNKNOWN;
    }

    scheduleSimulation(n);

    return nodes;
}

void fail(int nodeId, int token)
{
    int r = request(nodeId, token, 0);
    if (r != 0)
    {
        printf("[%4.1f] Não foi possível falhar o nodo %d\n", time(), token);
        fflush(stdout);
        exit(1);
    }
    printf("[%4.1f] Nodo %d falhou\n", time(), token);
    fflush(stdout);
}

void recover(int nodeId, int token)
{
    release(nodeId, token);
    nodes[nodeId].nextNodeState = UNKNOWN;
    printf("[%4.1f] Nodo %d recuperado\n", time(), token);
    fflush(stdout);
}

int isFaulty(int nodeId)
{
    return status(nodeId) != 0 ? 1 : 0;
}

int getNextToken(int currentToken, int numNodes)
{
    return (currentToken + 1) % numNodes;
}

int nextNodeId(int currentToken, int numNodes)
{
    return nodes[getNextToken(currentToken, numNodes)].id;
}

int getNodeId(int token)
{
    return nodes[token].id;
}

int getValidToken(int token, int numNodes)
{
    return (token) % numNodes;
}

void scheduleEvent(int evt, real _time, int token)
{
    numEvents++;
    schedule(evt, _time, token);
}

void scheduleTests(int n, real _time)
{
    for (int i = 0; i < n; i++)
    {
        scheduleEvent(TEST, _time, i);
    }
}

void scheduleSimulation(int numNodes)
{
    scheduleEvent(FAULT, 30.0, getValidToken(4, numNodes));
    scheduleEvent(FAULT, 40.0, getValidToken(5, numNodes));
    scheduleTests(numNodes, 45.0);
    scheduleEvent(FAULT, 53.0, getValidToken(7, numNodes));
    scheduleEvent(FAULT, 55.0, getValidToken(2, numNodes));
    scheduleEvent(REPAIR, 57.5, getValidToken(4, numNodes));
    scheduleTests(numNodes, 58.0);
    scheduleEvent(FAULT, 59.0, getValidToken(1, numNodes));
    scheduleEvent(REPAIR, 68.0, getValidToken(7, numNodes));
    scheduleEvent(FAULT, 130.0, getValidToken(4, numNodes));
    scheduleEvent(REPAIR, 155.0, getValidToken(4, numNodes));
    scheduleTests(numNodes, 180.0);
    scheduleEvent(REPAIR, 181.0, getValidToken(2, numNodes));
    scheduleEvent(REPAIR, 211.0, getValidToken(1, numNodes));
    scheduleEvent(REPAIR, 255.0, getValidToken(5, numNodes));
    scheduleTests(numNodes, 300.0);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        puts("Uso: ./tarefa_1 <num_nodos>\n\t<num_nodos>: nodos usados na simulação");
        exit(1);
    }

    const int n = atoi(argv[1]);
    int event, currentToken, nextToken, currentId, nextId;

    nodes = createSimulation("Teste em anel simples", n);

    for (int i = 0; i < numEvents; i++)
    {
        cause(&event, &currentToken);
        nextToken = getNextToken(currentToken, n);
        currentId = getNodeId(currentToken);
        nextId = nextNodeId(currentToken, n);
        switch (event)
        {
            case TEST:
                if (isFaulty(currentId)) break;
                if (isFaulty(nextId))
                {
                    nodes[currentToken].nextNodeState = FAULTY;
                    printf("[%4.1f] Nodo %d testou %d como falho\n", time(), currentToken, nextToken);
                    fflush(stdout);
                }
                else
                {
                    nodes[currentToken].nextNodeState = FAULT_FREE;
                    printf("[%4.1f] Nodo %d testou %d como sem-falha\n", time(), currentToken, nextToken);
                    fflush(stdout);
                }
                break;

            case FAULT:
                fail(getNodeId(currentToken), currentToken);
                break;

            case REPAIR:
                recover(getNodeId(currentToken), currentToken);
                break;
        }
    }
}