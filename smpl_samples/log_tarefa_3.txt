[15.0] Nodo 0 testou 1 como sem-falha
[15.0] Nodo 1 testou 2 como sem-falha
[15.0] Nodo 2 testou 3 como sem-falha
[15.0] Nodo 3 testou 4 como sem-falha
[15.0] Nodo 4 testou 5 como sem-falha
[15.0] Nodo 5 testou 6 como sem-falha
[15.0] Nodo 6 testou 7 como sem-falha
[15.0] Nodo 7 testou 0 como sem-falha
[STATE] 0: [ 0: FAULT_FREE, 1: FAULT_FREE, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 1: [ 0: UNKNOWN, 1: FAULT_FREE, 2: FAULT_FREE, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 2: [ 0: UNKNOWN, 1: UNKNOWN, 2: FAULT_FREE, 3: FAULT_FREE, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 3: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: FAULT_FREE, 4: FAULT_FREE, 5: UNKNOWN, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 4: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: FAULT_FREE, 5: FAULT_FREE, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 5: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: FAULT_FREE, 6: FAULT_FREE, 7: UNKNOWN ]
[STATE] 6: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: FAULT_FREE, 7: FAULT_FREE ]
[STATE] 7: [ 0: FAULT_FREE, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: FAULT_FREE ]
[30.0] Nodo 4 falhou
[40.0] Nodo 5 falhou
[45.0] Nodo 0 testou 1 como sem-falha
[45.0] Nodo 1 testou 2 como sem-falha
[45.0] Nodo 2 testou 3 como sem-falha
[45.0] Nodo 3 testou 4 como falho
[45.0] Nodo 3 testou 5 como falho
[45.0] Nodo 3 testou 6 como sem-falha
[45.0] Nodo 6 testou 7 como sem-falha
[45.0] Nodo 7 testou 0 como sem-falha
[53.0] Nodo 7 falhou
[55.0] Nodo 2 falhou
[57.5] Nodo 4 recuperado
[58.0] Nodo 0 testou 1 como sem-falha
[58.0] Nodo 1 testou 2 como falho
[58.0] Nodo 1 testou 3 como sem-falha
[58.0] Nodo 3 testou 4 como sem-falha
[58.0] Nodo 4 testou 5 como falho
[58.0] Nodo 4 testou 6 como sem-falha
[58.0] Nodo 6 testou 7 como falho
[58.0] Nodo 6 testou 0 como sem-falha
[58.0] Nodo 1 falhou
[68.0] Nodo 7 recuperado
[130.0] Nodo 4 falhou
[155.0] Nodo 4 recuperado
[180.0] Nodo 0 testou 1 como falho
[180.0] Nodo 0 testou 2 como falho
[180.0] Nodo 0 testou 3 como sem-falha
[180.0] Nodo 3 testou 4 como sem-falha
[180.0] Nodo 4 testou 5 como falho
[180.0] Nodo 4 testou 6 como sem-falha
[180.0] Nodo 6 testou 7 como sem-falha
[180.0] Nodo 7 testou 0 como sem-falha
[181.0] Nodo 2 recuperado
[211.0] Nodo 1 recuperado
[255.0] Nodo 5 recuperado
[300.0] Nodo 0 testou 1 como sem-falha
[300.0] Nodo 1 testou 2 como sem-falha
[300.0] Nodo 2 testou 3 como sem-falha
[300.0] Nodo 3 testou 4 como sem-falha
[300.0] Nodo 4 testou 5 como sem-falha
[300.0] Nodo 5 testou 6 como sem-falha
[300.0] Nodo 6 testou 7 como sem-falha
[300.0] Nodo 7 testou 0 como sem-falha
[330.0] Nodo 0 falhou
[330.0] Nodo 1 falhou
[330.0] Nodo 2 falhou
[330.0] Nodo 3 falhou
[330.0] Nodo 4 falhou
[330.0] Nodo 5 falhou
[330.0] Nodo 6 falhou
[330.0] Nodo 7 falhou
[331.0] Nodo 0 recuperado
[340.0] Nodo 0 testou 1 como falho
[340.0] Nodo 0 testou 2 como falho
[340.0] Nodo 0 testou 3 como falho
[340.0] Nodo 0 testou 4 como falho
[340.0] Nodo 0 testou 5 como falho
[340.0] Nodo 0 testou 6 como falho
[340.0] Nodo 0 testou 7 como falho
[340.0] Nodo 0 testou todos os nodos do sistema falhos
[STATE] 0: [ 0: FAULT_FREE, 1: FAULTY, 2: FAULTY, 3: FAULTY, 4: FAULTY, 5: FAULTY, 6: FAULTY, 7: FAULTY ]
[STATE] 1: [ 0: UNKNOWN, 1: FAULT_FREE, 2: FAULT_FREE, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 2: [ 0: UNKNOWN, 1: UNKNOWN, 2: FAULT_FREE, 3: FAULT_FREE, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: UNKNOWN ]
[STATE] 3: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: FAULT_FREE, 4: FAULT_FREE, 5: FAULTY, 6: FAULT_FREE, 7: UNKNOWN ]
[STATE] 4: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: FAULT_FREE, 5: FAULT_FREE, 6: FAULT_FREE, 7: UNKNOWN ]
[STATE] 5: [ 0: UNKNOWN, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: FAULT_FREE, 6: FAULT_FREE, 7: UNKNOWN ]
[STATE] 6: [ 0: FAULT_FREE, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: FAULT_FREE, 7: FAULT_FREE ]
[STATE] 7: [ 0: FAULT_FREE, 1: UNKNOWN, 2: UNKNOWN, 3: UNKNOWN, 4: UNKNOWN, 5: UNKNOWN, 6: UNKNOWN, 7: FAULT_FREE ]
