#!/bin/bash

for i in 4 8 16;
do
 ./vcube -g $i-nodes-global.log.txt -d $i-nodes-diagnostic.log.txt -m $i-nodes-messages.log.txt < tests/$i-nodes.in;
done;

cp *.log.txt html/logs/

rm *.log.txt

cp -r include/ obj/ src/ Makefile unit_tests.sh html/code/
