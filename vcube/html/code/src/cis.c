/**
 * @file cis.c
 * @author Elias P. Duarte Jr. (elias@inf.ufpr.br)
 * @brief versao adaptada do arquivo "cisj.c" disponivel na pagina do professor
 * @version 1.0
 * @date 2019-04-03
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cis.h"

#define POW_2(num) (1<<(num))
#define VALID_J(j, s) ((POW_2(s-1)) >= j)

/* |-- NodeSet.h */
NodeSet *createNodeSet(size_t size)
{
	NodeSet *new;

	new = (NodeSet *)malloc(sizeof(NodeSet));
	new->nodes = (int *)malloc(sizeof(int) * size);
	new->size = size;
	new->offset = 0;
	return new;
}

void NodeSet_insert(NodeSet *nodes, int node)
{
	if (nodes == NULL) return;
	nodes->nodes[nodes->offset++] = node;
}

void NodeSet_merge(NodeSet *dest, NodeSet *source)
{
	if (dest == NULL || source == NULL) return;
	memcpy(&(dest->nodes[dest->offset]), source->nodes, sizeof(int) * source->size);
	dest->offset += source->size;
}

void deleteNodeSet(NodeSet *nodes)
{
	free(nodes->nodes);
	free(nodes);
}
/* NodeSet.h --| */

NodeSet *cis(int i, int s)
{
	NodeSet *nodes, *other_nodes;
	int xor = i ^ POW_2(s-1);
	int j;

	/* starting node list */
	nodes = createNodeSet(POW_2(s-1));

	/* inserting the first value (i XOR 2^^(s-1)) */
	NodeSet_insert(nodes, xor);

	/* recursion */
	for (j = 1; j <= s-1; j++) {
		other_nodes = cis(xor, j);
		NodeSet_merge(nodes, other_nodes);
		deleteNodeSet(other_nodes);
	}
	return nodes;
}
