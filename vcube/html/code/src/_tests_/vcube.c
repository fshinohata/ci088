#include <stdio.h>
#include <assert.h>
#include "const.h"
#include "smpl.h"
#include "vcube.h"

int main(int argc, char *argv[])
{
    smpl(0, "Teste VCube");
    reset();
    stream(1);
    assert(uintlog2(64) == 6L);
    VCube *vcube = createVCube(64L);
    assert(vcube->logN == 6L);
    assert(vcube->n = 64L);
    return 0;
}
