/**
 * @file log.c
 * @author Fernando Aoyagui Shinohata (fas16@inf.ufpr.br)
 * @brief funcoes de log
 * @version 1.0
 * @date 2019-06-03
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "const.h"
#include "log.h"

static char buffer[1024];

struct Log
{
    FILE *global;
    FILE *diagnostic;
    FILE *message;
};

int isStandardInputOrOutput(FILE *f)
{
    return (f == stdout) || (f == stderr) || (f == stdin);
}

void deleteLog(Log *log)
{
    if (!isStandardInputOrOutput(log->global)) fclose(log->global);
    if (!isStandardInputOrOutput(log->diagnostic)) fclose(log->diagnostic);
    if (!isStandardInputOrOutput(log->message)) fclose(log->message);
    free(log);
}

Log *createLog(FILE *global, FILE *diagnostic, FILE *message)
{
    Log *log = malloc(sizeof(Log));
    if (!log) ERROR("Nao foi possivel alocar uma estrutura 'Log'.\n");

    log->global = global;
    log->diagnostic = diagnostic;
    log->message = message;

    return log;
}

void logAll(Log *log, const char *message, ...)
{
    va_list args;
    va_start(args, message);
    vsprintf(buffer, message, args);
    fputs(buffer, log->global);
    if (log->diagnostic != log->global)
    {
        fputs(buffer, log->diagnostic);
    }
    if (log->message != log->diagnostic && log->message != log->global)
    {
        fputs(buffer, log->message);
    }
}

void logMessage(Log *log, const char *message, ...)
{
    va_list args;
    va_start(args, message);
    vsprintf(buffer, message, args);
    fputs(buffer, log->global);
    if (log->message != log->global)
    {
        fputs(buffer, log->message);
    }
}

void logDiagnostic(Log *log, const char *message, ...)
{
    va_list args;
    va_start(args, message);
    vsprintf(buffer, message, args);
    fputs(buffer, log->global);
    if (log->diagnostic != log->global)
    {
        fputs(buffer, log->diagnostic);
    }
}