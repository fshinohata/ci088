#ifndef __SMPL_SIMULATION__
#define __SMPL_SIMULATION__

#include <stdint.h>
#include "vcube.h"
#include "log.h"

typedef enum EVENT_TYPE EVENT_TYPE;

typedef struct SimulationEvent SimulationEvent;
typedef struct Simulation Simulation;

/**
 * @brief tipos de evento do SMPL
 *        nao tem relacao com os eventos da simulacao!
 *        os eventos do SMPL sao os agendamentos temporais (o que ocorre quando)
 *
 *        os eventos da simulacao sao falhas e recuperacoes,
 *        enquanto eventos do SMPL sao agendamentos de falhas,
 *        recuperacoes e testes!
 */
enum EVENT_TYPE
{
    EVENT_TYPE_NONE,
    EVENT_TYPE_TEST_INTERVAL, /** inicia intervalo de testes no vcube */
    EVENT_TYPE_FAULT, /** causa falha de um nodo */
    EVENT_TYPE_RECOVER, /** causa recuperacao de um nodo */
    EVENT_TYPE_BROADCAST_SEND,
    EVENT_TYPE_BROADCAST_RECEIVE,
    EVENT_TYPE_SAVE_EVENT,
    EVENT_TYPE_CHECK_LATENCY,
    EVENT_TYPE_RESET_LATENCY,
};

struct SimulationEvent
{
    EVENT_TYPE type;
    int *tokens;
    int tokensLength;
    int tokensAllocated;
};

/**
 * @brief estrutura de simulacao
 */
struct Simulation
{
    VCube *vcube; /** vcube */
    long n;
    SimulationEvent *lastEvent;
    SimulationEvent *events;
    long eventsLength;
    long eventsAllocated;
    long smplEvents;
    Log *log;
};

void deleteSimulation(Simulation *s);
Simulation *createSimulation(char *argv[], int argc, long n);
void runSimulation(Simulation *s);
void logSimulation(Simulation *s);

#endif
