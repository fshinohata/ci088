#ifndef __VCUBE__
#define __VCUBE__

#include <stdint.h>
#include "const.h"
#include "log.h"

typedef struct VCube VCube;
typedef struct VCubeNode VCubeNode;

/**
 * @brief estrutura de um nodo do VCube
 */
struct VCubeNode
{
    int id; /** id (facility()) do nodo */
    int token; /** token do nodo (indice no vetor) */
    int *testSubjects; /** sujeitos de teste do nodo (quem ele testa) */
    long testSubjectsLength; /** numero de sujeitos de teste*/

    int *STATE; /** vetor STATE */
};

/**
 * @brief estrutura do VCube
 */
struct VCube
{
    VCubeNode *nodes; /** nodos do VCube */
    long n; /** numero de nodos */
    long logN; /** log n (pre-calculado) */
    int *cis;
    Log *log;
};

int uintlog2(int n);

int isTrulyFaulty(VCubeNode *node);
int isTrulyFaultFree(VCubeNode *node);
int isFaulty(VCubeNode *node, int token);
int isFaultFree(VCubeNode *node, int token);
int isUnknown(VCubeNode *node, int token);

void printAllTestSubjects(VCube *vcube);
void runTests(VCube *vcube, int i);
void calculateTestSubjects(VCube *vcube, int token);

void decryptClusterAndToken(int encrypted, int n, int *cluster, int *token);
int getFirstFaultFreeNodeOfEachCluster(VCube *vcube, int token, int lastCluster, int *buffer);

void deleteVCube(VCube *vcube);
VCube *createVCube(long n, Log *log);

#endif
