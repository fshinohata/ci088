#ifndef __LOG_MANAGER__
#define __LOG_MANAGER__

typedef struct Log Log;

void deleteLog(Log *log);
Log *createLog(FILE *global, FILE *diagnostic, FILE *message);
void logAll(Log *log, const char *message, ...);
void logMessage(Log *log, const char *message, ...);
void logDiagnostic(Log *log, const char *message, ...);

#endif