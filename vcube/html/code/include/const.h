#ifndef __COMMON_CONSTANTS__
#define __COMMON_CONSTANTS__

#include <stdio.h>

#define VCUBE_MAXIMUM_DIMENSION 64

#define NODE_STATE_UNKNOWN -1L
#define NODE_STATE_INITIAL_FAULT_FREE 0L
#define NODE_STATE_INITIAL_FAULTY 1L

#define IS_STATE_FAULTY(n) (((n) & 1) == 1)
#define IS_STATE_FAULT_FREE(n) ((n & 1) == 0)
#define IS_STATE_UNKNOWN(n) ((n) == -1)

#define STRING_SIZE 128
#define COMMENT '#'

// #define free(ptr) {printf("FREEING " #ptr " <%p>: ", ptr); fflush(stdout); free(ptr); puts("DONE"); fflush(stdout);}

#ifdef DEBUG
    #define VERBOSE(...) fprintf(stderr, __VA_ARGS__)
#else
    #define VERBOSE(...) {}
#endif

#define ERROR(...) { fprintf(stderr, __VA_ARGS__); exit(1); }

#endif
