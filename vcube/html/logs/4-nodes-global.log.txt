NODOS: 4
EVENTOS:
	INTERVALO DE TESTES
	INTERVALO DE TESTES
	BROADCAST: 0 
	BROADCAST: 1 
	BROADCAST: 2 
	BROADCAST: 3 
	FALHA: 0 
	INTERVALO DE TESTES
	INTERVALO DE TESTES
	BROADCAST: 1 
	BROADCAST: 2 
	BROADCAST: 3 
[ 0.0] [TESTES] 0: |1:0|2:0|   STATE: [0:0|1:0|2:0|3:-1]
[ 0.0] [TESTES] 1: |0:0|3:0|   STATE: [0:0|1:0|2:0|3:0]
[ 0.0] [TESTES] 2: |0:0|3:0|   STATE: [0:0|1:0|2:0|3:0]
[ 0.0] [TESTES] 3: |1:0|2:0|   STATE: [0:0|1:0|2:0|3:0]
[10.0] [TESTES] 0: |1:0|2:0|   STATE: [0:0|1:0|2:0|3:0]
[10.0] [TESTES] 1: |0:0|3:0|   STATE: [0:0|1:0|2:0|3:0]
[10.0] [TESTES] 2: |0:0|3:0|   STATE: [0:0|1:0|2:0|3:0]
[10.0] [TESTES] 3: |1:0|2:0|   STATE: [0:0|1:0|2:0|3:0]
estado de todos os nodos diagnosticado (latencia: 2)
[20.0] [BROADCAST] 0: SEND(1,2,)
[21.0] [RECEIVE] 1:(src=0,s=1) 
[21.0] [RECEIVE] 2:(src=0,s=2) PROPAGATE(3,)
[22.0] [RECEIVE] 3:(src=2,s=1) 
[30.0] [BROADCAST] 1: SEND(0,3,)
[31.0] [RECEIVE] 0:(src=1,s=1) 
[31.0] [RECEIVE] 3:(src=1,s=2) PROPAGATE(2,)
[32.0] [RECEIVE] 2:(src=3,s=1) 
[40.0] [BROADCAST] 2: SEND(3,0,)
[41.0] [RECEIVE] 3:(src=2,s=1) 
[41.0] [RECEIVE] 0:(src=2,s=2) PROPAGATE(1,)
[42.0] [RECEIVE] 1:(src=0,s=1) 
[50.0] [BROADCAST] 3: SEND(2,1,)
[51.0] [RECEIVE] 2:(src=3,s=1) 
[51.0] [RECEIVE] 1:(src=3,s=2) PROPAGATE(0,)
[52.0] [RECEIVE] 0:(src=1,s=1) 
[60.0] [FALHA] 0
[70.0] [TESTES] 1: |0:1|3:0|   STATE: [0:1|1:0|2:0|3:0]
[70.0] [TESTES] 2: |0:1|3:0|   STATE: [0:1|1:0|2:0|3:0]
[70.0] [TESTES] 3: |1:0|2:0|   STATE: [0:1|1:0|2:0|3:0]
ultimo evento descoberto (latencia: 1)
[80.0] [TESTES] 1: |0:1|2:0|3:0|   STATE: [0:1|1:0|2:0|3:0]
[80.0] [TESTES] 2: |0:1|3:0|   STATE: [0:1|1:0|2:0|3:0]
[80.0] [TESTES] 3: |1:0|2:0|   STATE: [0:1|1:0|2:0|3:0]
[90.0] [BROADCAST] 1: SEND(3,)
[91.0] [RECEIVE] 3:(src=1,s=2) PROPAGATE(2,)
[92.0] [RECEIVE] 2:(src=3,s=1) 
[100.0] [BROADCAST] 2: SEND(3,1,)
[101.0] [RECEIVE] 3:(src=2,s=1) 
[101.0] [RECEIVE] 1:(src=2,s=2) PROPAGATE()
[110.0] [BROADCAST] 3: SEND(2,1,)
[111.0] [RECEIVE] 2:(src=3,s=1) 
[111.0] [RECEIVE] 1:(src=3,s=2) PROPAGATE()
