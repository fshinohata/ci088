/* global hljs */
(function () {
    $(".ui.modal").modal();
    $(".ui.dropdown").dropdown();

    function escapeHTML(html) {
        html = html.replace(/[<]/g, "&lt;");
        html = html.replace(/[>]/g, "&gt;");
        return html;
    }

    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            var el = document.querySelector(this.getAttribute("href"));
            document.body.scrollTo({
                behavior: "smooth",
                top: el.offsetTop - 80
            });
        });
    });
    document.querySelectorAll("a[data-load]").forEach(link => {
        link.addEventListener("click", function (e) {
            e.preventDefault();
            var location = window.location.href.replace("index.html", "");
            if (!location.endsWith("/")) location = location + "/";
            var type = "plaintext";

            if (this.dataset.load.endsWith(".c")) type = "c";
            if (this.dataset.load.endsWith(".sh")) type = "bash";
            if (this.dataset.load.endsWith("Makefile")) type = "makefile";

            $.ajax({
                url: location + this.dataset.load,
                success: function (data) {
                    console.log({data});
                    var block = document.getElementById("code-holder");
                    block.className = type;
                    block.innerHTML = escapeHTML(data);
                    hljs.highlightBlock(block);
                    $("#code-modal").modal("show");
                },
                error: function (e) {
                    console.error(e);
                }
            })
        });
    });
})();
