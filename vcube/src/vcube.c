/**
 * @file vcube.c
 * @author Fernando Aoyagui Shinohata (fas16@inf.ufpr.br)
 * @brief funcoes de gerenciamento do vcube (estruturas e calculo de testes)
 * @version 1.0
 * @date 2019-06-05
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "const.h"
#include "cis.h"
#include "log.h"
#include "smpl.h"
#include "vcube.h"

/**
 * @brief retorna teto(log2 N)
 *
 * @param n         numero para calcular o log2
 * @return int teto(log2 n)
 */
int uintlog2(int n)
{
    int log = 0;
    int bits1 = 0;
    while (n > 1L)
    {
        bits1 += (n & 1L);
        n >>= 1;
        log++;
    }
    if (bits1 >= 1)
        return log + 1;
    return log;
}

/**
 * @brief retorna se o nodo esta falho de verdade
 *        usado para nao fazer nada com nodos falhos
 *
 * @param node      nodo para verificar o estado
 * @return int      1 se falho, 0 se sem-falha
 */
int isTrulyFaulty(VCubeNode *node)
{
    return status(node->id) != 0 ? 1 : 0;
}

/**
 * @brief inverso do isTrulyFaulty()
 *
 * @param node      nodo para verificar o estado
 * @return int      1 se sem-falha, 0 se falho
 */
int isTrulyFaultFree(VCubeNode *node)
{
    return !isTrulyFaulty(node);
}

/**
 * @brief retorna se o nodo [token] esta falho na
 *        perspectiva do nodo [node].
 *
 * @param node      nodo-perspectiva
 * @param token     token do nodo para verificar
 * @return int      1 se falho, 0 se sem-falha
 */
int isFaulty(VCubeNode *node, int token)
{
    return IS_STATE_FAULTY(node->STATE[token]) && !IS_STATE_UNKNOWN(node->STATE[token]);
}

/**
 * @brief inverso de isFaulty()
 *
 * @param node      nodo-perspectiva
 * @param token     token do nodo para verificar
 * @return int      1 se sem-falha, 0 se falho
 */
int isFaultFree(VCubeNode *node, int token)
{
    return !isFaulty(node, token);
}

/**
 * @brief retorna se o estado do nodo [token] nao eh
 *        conhecido pelo nodo [node].
 *
 * @param node      nodo-perspectiva
 * @param token     token do nodo para verificar
 * @return int      1 se sim, 0 caso contrario
 */
int isUnknown(VCubeNode *node, int token)
{
    return IS_STATE_UNKNOWN(node->STATE[token]);
}

/**
 * @brief limpa uma estrutura <VCubeNode>
 *        nao desaloca o ponteiro, apenas as estruturas internas!
 *
 * @param node      nodo para "limpar"
 */
static void cleanVCubeNode(VCubeNode *node)
{
    free(node->testSubjects);
    free(node->STATE);
}

/**
 * @brief inicializa um nodo do VCube baseado em seu ID
 *
 * @param node      nodo a ser inicializado
 * @param id        id do nodo
 * @param n         numero total de nodos
 * @return int      0 em sucesso, 1 em falha
 */
static void _initVCubeNode(VCubeNode *node, int token, long n)
{
    static char fa_name[5];

    memset(fa_name, '\0', 5);
    sprintf(fa_name, "%d", token);
    node->id = facility(fa_name, 1);
    node->token = token;

    node->testSubjects = (int *)malloc(n * sizeof(int));
    if (!node->testSubjects) ERROR("Nao foi possivel alocar o vetor 'VCubeNode::testSubjects' para o token '%d'.\n", token);

    node->testSubjectsLength = 0L;

    node->STATE = (int *)malloc(n * sizeof(int));
    if (!node->STATE) ERROR("Nao foi possivel alocar o vetor 'VCubeNode::STATE' para o token '%d'\n", token);

    for (int i = 0; i < n; i++) node->STATE[i] = NODE_STATE_UNKNOWN;
    node->STATE[token] = NODE_STATE_INITIAL_FAULT_FREE;
}

/**
 * @brief imprime todos os sujeitos de teste do nodo (quem ele vai testar)
 *
 * @param node      nodo-alvo
 */
static void _printTestSubjects(VCubeNode *node, Log *log)
{
    if (isTrulyFaulty(node)) return;
    logDiagnostic(log, "[%d]:[", node->token);
    logDiagnostic(log, "%d", node->testSubjects[0]);
    for (long i = 1; i < node->testSubjectsLength; i++)
    {
        logDiagnostic(log, ", %d", node->testSubjects[i]);
    }
    logDiagnostic(log, "]\n");
}

/**
 * @brief imprime todos os sujeitos de testes de todos os nodos do VCube
 *
 * @param vcube     vcube-alvo
 */
void printAllTestSubjects(VCube *vcube)
{
    logDiagnostic(vcube->log, "GRAFO DE TESTES:");
    for (long i = 0; i < vcube->n; i++)
        _printTestSubjects(&vcube->nodes[i], vcube->log);
    logDiagnostic(vcube->log, "");
}

/**
 * @brief verifica se o nodo [token] eh um sujeito de teste
 *        do nodo [node]
 *
 * @param node      nodo alvo
 * @param token     token para verificar
 * @return int      1 se sim, 0 caso contrario
 */
static int _isTestSubject(VCubeNode *node, int token)
{
    for (int i = 0; i < node->testSubjectsLength; i++)
    {
        if (node->testSubjects[i] == token) return 1;
    }
    return 0;
}

/**
 * @brief calcula cis(i,s) e armazena em _cis
 *
 * @param _cis          vetor para armazenar resultado
 * @param i             nodo i
 * @param s             cluster s
 * @return long       quantas entradas foram adicionadas
 */
static long _fillCIS(int *_cis, int i, int s)
{
    NodeSet *cisSet = cis(i, s);
    long offset = cisSet->offset;

    for (int k = 0; k < offset; k++)
    {
        _cis[k] = cisSet->nodes[k];
    }
    _cis[offset] = -1;

    deleteNodeSet(cisSet);

    return offset;
}

/**
 * @brief calcula e preenche o vetor *_cis com as listas c(i,s)
 *
 * @param _cis      referencia do vetor para preencher
 * @param n         numero de nodos
 * @param logN      log2(n)
 */
static void _calculateCIS(int *_cis, int n, int logN)
{
    int d1 = logN * n;
    int d2 = logN;
    int offset;

    for (int i = 0; i < n; i++)
    {
        for (int s = 1; s <= logN; s++)
        {
            /* offset no vetor */
            offset = (i * d1) + ((s - 1) * d2);
            _fillCIS(&_cis[offset], i, s);
        }
    }
}

/**
 * @brief verifica se o nodo [tester] deve testar [who]
 *
 * @param tester    nodo testador
 * @param cis       matriz c(i,s)
 * @param who       quem verificar
 * @param s         cluster de [who]
 * @param n         numero de nodos
 * @param logN      log2(n)
 * @return int      1 se sim, 0 caso contrario
 */
static int _shouldTest(VCubeNode *tester, int *cis, int who, int s, int n, int logN)
{
    int d1 = logN * n;
    int d2 = logN;
    int offset = (who * d1) + ((s - 1) * d2);
    while (cis[offset] != -1)
    {
        if (cis[offset] == tester->token) return 1;
        if (isFaultFree(tester, cis[offset])) return 0;
        offset++;
    }
    return 0;
}

/**
 * @brief calcula os sujeitos de teste do nodo [tester]
 *
 * @param tester    nodo testador
 * @param cis       matriz c(i, s)
 * @param n         numero de nodos
 * @param logN      log2(n)
 */
static void _calculateTestSubjects(VCubeNode *tester, int *cis, int n, int logN)
{
    tester->testSubjectsLength = 0;
    for (int i = 0; i < n; i++)
    {
        if (i == tester->token) continue;
        for (int s = 1; s <= logN; s++)
        {
            if (_shouldTest(tester, cis, i, s, n, logN))
            {
                tester->testSubjects[tester->testSubjectsLength] = i;
                tester->testSubjectsLength++;
                break;
            }
        }
    }
}

/**
 * @brief interface do header (.h) para chamar _calculateTestSubjects() "de fora"
 *
 * @param vcube     estrutura VCube
 * @param token     token do nodo cujos sujeitos de teste devem ser calculados
 */
void calculateTestSubjects(VCube *vcube, int token)
{
    _calculateTestSubjects(&vcube->nodes[token], vcube->cis, vcube->n, vcube->logN);
}

/**
 * @brief nome auto-explicativo.
 *
 * @param tester    nodo testador
 * @param cis       matriz c(i, s)
 * @param i         token do nodo testador
 * @param s         cluster do nodo
 * @param n         numero de nodos
 * @param logN      log2(n)
 * @return int      retorna o token do primeiro nodo do cluster, ou -1 se nao houver
 */
static int _getFirstFaultFreeNodeOfCluster(VCubeNode *tester, int *cis, int i, int s, int n, int logN)
{
    int d1 = logN * n;
    int d2 = logN;
    int offset = (i * d1) + ((s - 1) * d2);
    while (cis[offset] != -1)
    {
        if (cis[offset] == tester->token) return tester->token;
        if (isFaultFree(tester, cis[offset])) return cis[offset];
        offset++;
    }
    return -1;
}

/**
 * @brief salva o cluster e o token dentro de um int
 *
 * @param cluster       cluster para salvar
 * @param token         token para salvar
 * @param n             numero de nodos
 * @return int          cluster e token "criptografados"
 */
int _encryptClusterAndToken(int cluster, int token, int n)
{
    return (cluster * n) + token;
}

/**
 * @brief inverso de _encryptClusterAndToken()
 * 
 * @param encrypted     mensagem criptografada
 * @param n             numero de nodos
 * @param cluster       referencia para salvar o cluster
 * @param token         referencia para salvar o token
 */
void decryptClusterAndToken(int encrypted, int n, int *cluster, int *token)
{
    *cluster = encrypted / n;
    *token = encrypted % n;
}

/**
 * @brief nome autoexplicativo.
 *        preenche o vetor [buffer] com o primeiro nodo sem falha de cada cluster
 *        (na perspectiva do nodo [token]).
 *        calcula e preenche o vetor ate o cluster [lastCluster].
 *        se o cluster i nao tiver nodos sem-falha, preenche buffer[i] com -1
 *
 * @param vcube         estrutura VCube
 * @param token         token do nodo-perspectiva
 * @param lastCluster   cluster limite para calcular
 * @param buffer        buffer para preencher
 * @return int          quantos clusters foram calculados
 */
int getFirstFaultFreeNodeOfEachCluster(VCube *vcube, int token, int lastCluster, int *buffer)
{
    VCubeNode *node = &vcube->nodes[token];
    int i = 0;
    int k;

    if (lastCluster > vcube->logN) ERROR("ERRO: [getFirstFaultFreeNodesOfEachCluster] lastCluster > vcube->logN\n");
    if (lastCluster <= 0) return 0;

    for (int s = 1; s <= lastCluster; s++)
    {
        k = _getFirstFaultFreeNodeOfCluster(node, vcube->cis, node->token, s, vcube->n, vcube->logN);
        if (k >= 0)
        {
            buffer[i++] = _encryptClusterAndToken(s, k, vcube->n);
        }
        else
        {
            buffer[i++] = -1;
        }
    }

    return i;
}

/**
 * @brief executa todos os testes que o nodo i deve realizar
 *
 * @param vcube     instancia do VCube a ser usada
 * @param i         indice do nodo testador
 */
void runTests(VCube *vcube, int i)
{
    VCubeNode *tester = &vcube->nodes[i];
    if (isTrulyFaulty(tester)) return;

    logDiagnostic(vcube->log, "[%4.1f] [TESTES] %d: |", time(), i);
    int mustRecalculateTestSubjects = 0;
    for (int k = 0; k < tester->testSubjectsLength; k++)
    {
        int j = tester->testSubjects[k];
        VCubeNode *tested = &vcube->nodes[j];
        if (isTrulyFaulty(tested))
        {
            if (tester->STATE[j] == NODE_STATE_UNKNOWN)
            {
                tester->STATE[j] = NODE_STATE_INITIAL_FAULTY;
                mustRecalculateTestSubjects = 1;
            }
            else if (IS_STATE_FAULT_FREE(tester->STATE[j]))
            {
                tester->STATE[j]++;
                mustRecalculateTestSubjects = 1;
            }
        }
        else
        {
            if (tester->STATE[j] == NODE_STATE_UNKNOWN)
            {
                tester->STATE[j] = NODE_STATE_INITIAL_FAULT_FREE;
                mustRecalculateTestSubjects = 1;
            }
            else if (IS_STATE_FAULTY(tester->STATE[j]))
            {
                tester->STATE[j]++;
                mustRecalculateTestSubjects = 1;
            }

            /* pega informacoes atualizadas de diagnostico */
            for (long m = 0; m < vcube->n; m++)
            {
                if (m != i && !_isTestSubject(tester, m) && tester->STATE[m] < tested->STATE[m])
                {
                    tester->STATE[m] = tested->STATE[m];
                }
            }
        }
        logDiagnostic(vcube->log, "%d:%d|", j, tester->STATE[j]);
    }

    if (mustRecalculateTestSubjects)
    {
        _calculateTestSubjects(tester, vcube->cis, vcube->n, vcube->logN);
    }

    logDiagnostic(vcube->log, "   STATE: [%d:%d", 0, tester->STATE[0L]);
    for (int k = 1; k < vcube->n; k++)
        logDiagnostic(vcube->log, "|%d:%d", k, tester->STATE[k]);
    logDiagnostic(vcube->log, "]\n");
}

/**
 * @brief desaloca uma estrutura <VCube>
 *
 * @param vcube     vcube-alvo
 */
void deleteVCube(VCube *vcube)
{
    for (int i = 0; i < vcube->n; i++) cleanVCubeNode(&vcube->nodes[i]);
    free(vcube->nodes);
    free(vcube->cis);
    free(vcube);
}

/**
 * @brief aloca uma estrutura <VCube>
 *
 * @param n         numero de nodos a compor o "VCube"
 * @return VCube*   estrutura alocada com n nodos
 */
VCube *createVCube(long n, Log *log)
{
    VCube *vcube = (VCube *)malloc(sizeof(VCube));
    if (vcube == NULL) ERROR("Nao foi possivel alocar espaco para um VCube.\n");

    vcube->nodes = (VCubeNode *)malloc(n * sizeof(VCubeNode));
    if (vcube->nodes == NULL) ERROR("Nao foi possivel alocar o vetor 'VCube::nodes' de tamanho '%ld'\n", n);

    vcube->log = log;

    vcube->n = n;
    vcube->logN = uintlog2(n);

    vcube->cis = (int *)malloc(vcube->n * vcube->logN * vcube->n * sizeof(int));
    if (!vcube->cis) ERROR("Nao foi possivel alocar o vetor 'VCube::cis'.\n");

    _calculateCIS(vcube->cis, vcube->n, vcube->logN);

    for (int i = 0; i < n; i++)
    {
        _initVCubeNode(&vcube->nodes[i], i, vcube->n);
        _calculateTestSubjects(&vcube->nodes[i], vcube->cis, vcube->n, vcube->logN);
    }

    return vcube;
}
