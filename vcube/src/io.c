/**
 * @file io.c
 * @author Fernando Aoyagui Shinohata (fas16@inf.ufpr.br)
 * @brief funcoes de leitura e tratamento do input
 * @version 1.0
 * @date 2019-06-03
 *
 */
#include <stdio.h>
#include <string.h>
#include "const.h"

#define LINE_END '\n'

/* headers */
static int _getOptionIndex(const char *option, char *argv[], int argc);
char *getOptionValue(const char *option, char *argv[], int argc);
int hasOption(const char *option, char *argv[], int argc);
static int _isWhiteSpace(char c);
static int _isLetter(char c);
static int _isDigit(char c);
int isWord(char *w, char comment);
int isNumber(char *n, char comment);
int getNextValidLine(FILE *in, char *buffer, char comment);

static int _getOptionIndex(const char *option, char *argv[], int argc)
{
    for (int i = 0; i < argc; i++)
    {
        if (0 == strcmp(option, argv[i]))
        {
            return i;
        }
    }
    return -1;
}

char *getOptionValue(const char *option, char *argv[], int argc)
{
    int index = _getOptionIndex(option, argv, argc);
    if (index >= 0 && index + 1 < argc)
    {
        return argv[index + 1];
    }
    return NULL;
}

int hasOption(const char *option, char *argv[], int argc)
{
    return _getOptionIndex(option, argv, argc) >= 0;
}

static int _isWhiteSpace(char c)
{
    return c == ' ' || c == '\n' || c == '\t' || c == '\r';
}

static int _isLetter(char c)
{
    return (65 <= c && c <= 90) || (97 <= c && c <= 122);
}

static int _isDigit(char c)
{
    return (48 <= c && c <= 57);
}

int isWord(char *w, char comment)
{
    while (*w)
    {
        if (!_isLetter(*w) || *w == comment) return 0;
        w++;
    }
    return 1;
}

int isNumber(char *n, char comment)
{
    while (*n)
    {
        if (!_isDigit(*n) || *n == comment) return 0;
        n++;
    }
    return 1;
}

int getNextValidLine(FILE *in, char *buffer, char comment)
{
    char c = fgetc(in);
    while (!feof(in) && (_isWhiteSpace(c) || c == comment))
    {
        if (c == comment)
        {
            while (c != LINE_END && c != EOF) c = fgetc(in);
        }
        c = fgetc(in);
    }

    int i = 0;
    while (!feof(in) && c != LINE_END)
    {
        buffer[i++] = c;
        c = fgetc(in);
    }

    buffer[i] = '\0';

    return i;
}
