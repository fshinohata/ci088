/**
 * @file main.c
 * @author Fernando Aoyagui Shinohata (fas16@inf.ufpr.br)
 * @brief funcao principal do trabalho 1 de ci088 (2019/1)
 * @version 1.0
 * @date 2019-04-20
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "const.h"
/**
 * @file main.c
 * @author Fernando Aoyagui Shinohata (fas16@inf.ufpr.br)
 * @brief main.
 * @version 1.0
 * @date 2019-06-03
 *
 */
#include "smpl.h"
#include "io.h"
#include "simulation.h"

/**
 * @brief reinicia/inicializa o SMPL
 */
void resetSMPL()
{
    smpl(0, "Simulacao");
    reset();
    stream(1);
}

int main(int argc, char *argv[])
{
    char line[STRING_SIZE];
    memset(line, '\0', STRING_SIZE);

    getNextValidLine(stdin, line, COMMENT);
    long n = (long)atol(line);

    if (n < 1L) ERROR("O numero de nodos deve ser no minimo 1!\n");

    resetSMPL();
    Simulation *s = createSimulation(argv, argc, n);
    logSimulation(s);
    runSimulation(s);
    deleteSimulation(s);

    return 0;
}
