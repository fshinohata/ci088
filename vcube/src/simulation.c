/**
 * @file simulation.c
 * @author Fernando Aoyagui Shinohata (fas16@inf.ufpr.br)
 * @brief funcoes de gerenciamento da simulacao do vcube com smpl
 * @version 1.0
 * @date 2019-06-05
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "const.h"
#include "io.h"
#include "log.h"
#include "simulation.h"
#include "smpl.h"
#include "vcube.h"

#define COMMAND_TEST "t"
#define COMMAND_FAIL "f"
#define COMMAND_RECOVER "r"
#define COMMAND_BROADCAST "b"
#define COMMAND_SEPARATOR " "
#define INITIAL_ARRAY_SIZE 16
#define INITIAL_SIMULATION_EVENTS 64

/* headers */
static void _cleanSimulationEvent(SimulationEvent *evt);
static void _initSimulationEvent(SimulationEvent *evt);
void SimulationEvent_addToken(SimulationEvent *evt, int token);
SimulationEvent *Simulation_addEvent(Simulation *s, EVENT_TYPE type);
void readEvents(Simulation *s, FILE *in);
Log *getSimulationLog(char *argv[], int argc);
void deleteSimulation(Simulation *s);
Simulation *createSimulation(char *argv[], int argc, long n);
void logSimulation(Simulation *s);
static void _scheduleSimulationEvents(Simulation *s);
static int _eventMatchesState(SimulationEvent *evt, VCubeNode *node);
static int _lastEventDiscovered(Simulation *s);
void runSimulation(Simulation *s);
static int _encrypt(int sender, int cluster, int target, int n);
static void _broadcast(Simulation *s, int token);
static void _decrypt(int message, int n, int *sender, int *cluster, int *target);
static void _receive(Simulation *s, int message);
static void _fail(Simulation *s, int token);
static void _recover(Simulation *s, int token);

/**
 * @brief limpa uma estrutura SimulationEvent
 *
 * @param evt   referencia da estrutura para limpar
 */
static void _cleanSimulationEvent(SimulationEvent *evt)
{
    free(evt->tokens);
}

/**
 * @brief inicializa uma estrutura SimulationEvent
 *
 * @param evt   referencia da estrutura para inicializar
 */
static void _initSimulationEvent(SimulationEvent *evt)
{
    evt->tokens = (int *)malloc(INITIAL_ARRAY_SIZE * sizeof(int));
    if (!evt->tokens) ERROR("Nao foi possivel alocar o vetor 'SimulationEvent::tokens'.\n");

    evt->tokensLength = 0;
    evt->tokensAllocated = INITIAL_ARRAY_SIZE;
    evt->type = EVENT_TYPE_NONE;
}

/**
 * @brief adiciona um token na lista de tokens do evento
 *
 * @param evt       evento alvo
 * @param token     token para adicionar
 */
void SimulationEvent_addToken(SimulationEvent *evt, int token)
{
    if (evt->tokensLength == evt->tokensAllocated)
    {
        evt->tokensAllocated <<= 1;
        evt->tokens = (int *)realloc(evt->tokens, evt->tokensAllocated * sizeof(int));
        if (!evt->tokens) ERROR("Nao foi possivel realocar o vetor 'SimulationEvent::tokens' (tamanho desejado: %d).\n", evt->tokensAllocated);
    }

    evt->tokens[evt->tokensLength++] = token;
}

/**
 * @brief adiciona um evento na lista de eventos da simulacao
 *
 * @param s                     simulacao
 * @param type                  tipo de evento
 * @return SimulationEvent*     estrutura SimulationEvent pre-inicializada
 */
SimulationEvent *Simulation_addEvent(Simulation *s, EVENT_TYPE type)
{
    if (s->eventsLength == s->eventsAllocated)
    {
        s->eventsAllocated <<= 1;
        s->events = (SimulationEvent *)realloc(s->events, s->eventsAllocated * sizeof(SimulationEvent));
        if (!s->events) ERROR("Nao foi possivel realocar o vetor 'Simulation::events' (tamanho desejado: %ld).\n", s->eventsAllocated);
    }

    s->events[s->eventsLength].type = type;
    s->eventsLength++;
    return &s->events[s->eventsLength - 1];
}

/**
 * @brief realiza a leitura dos eventos do arquivo de entrada
 *
 * @param s     simulacao
 * @param in    arquivo de entrada
 */
void readEvents(Simulation *s, FILE *in)
{
    char line[STRING_SIZE];
    memset(line, '\0', STRING_SIZE);

    char *event;
    char *token;

    while (!feof(in))
    {
        int read = getNextValidLine(in, line, COMMENT);

        if (read == 0) continue;

        event = strtok(line, COMMAND_SEPARATOR);

        if (0 == strcmp(event, COMMAND_TEST))
        {
            SimulationEvent *evt = Simulation_addEvent(s, EVENT_TYPE_TEST_INTERVAL);

            for (int k = 0; k < s->n; k++)
            {
                SimulationEvent_addToken(evt, k);
            }
        }
        else if (0 == strcmp(event, COMMAND_FAIL))
        {
            SimulationEvent *evt = Simulation_addEvent(s, EVENT_TYPE_FAULT);
            token = NULL;
            do
            {
                token = strtok(NULL, COMMAND_SEPARATOR);
                if (token)
                {
                    SimulationEvent_addToken(evt, atoi(token));
                }
            }
            while (token);
            if (evt->tokensLength == 0) ERROR("Voce deve falhar pelo menos um nodo para criar um evento de falha.\n");
        }
        else if (0 == strcmp(event, COMMAND_RECOVER))
        {
            SimulationEvent *evt = Simulation_addEvent(s, EVENT_TYPE_RECOVER);
            token = NULL;
            do
            {
                token = strtok(NULL, COMMAND_SEPARATOR);
                if (token)
                {
                    SimulationEvent_addToken(evt, atoi(token));
                }
            }
            while (token);
            if (evt->tokensLength == 0) ERROR("Voce deve recuperar pelo menos um nodo para criar um evento de recuperacao.\n");
        }
        else if (0 == strcmp(event, COMMAND_BROADCAST))
        {
            SimulationEvent *evt = Simulation_addEvent(s, EVENT_TYPE_BROADCAST_SEND);
            token = strtok(NULL, COMMAND_SEPARATOR);
            if (token)
            {
                SimulationEvent_addToken(evt, atoi(token));
            }
            else
            {
                ERROR("Um broadcast deve ser feito por exatamente um nodo.\n");
            }
        }
    }
}

/**
 * @brief analisa [argv] e cria uma estrutura Log para a simulacao
 *
 * @param argv      argv
 * @param argc      argc
 * @return Log*     estrutura Log inicializada
 */
Log *getSimulationLog(char *argv[], int argc)
{
    FILE *g = stdout;
    FILE *d = stdout;
    FILE *m = stdout;

    char *mode = "w";

    char *f;
    if ((f = getOptionValue("-a", argv, argc)))
    {
        mode = "a";
    }

    if ((f = getOptionValue("-g", argv, argc)))
    {
        g = fopen(f, mode);
    }

    if ((f = getOptionValue("-d", argv, argc)))
    {
        d = fopen(f, mode);
    }

    if ((f = getOptionValue("-m", argv, argc)))
    {
        m = fopen(f, mode);
    }

    return createLog(g, d, m);
}

/**
 * @brief desaloca uma estrutura Simulation
 *
 * @param s         simulacao-alvo
 */
void deleteSimulation(Simulation *s)
{
    deleteVCube(s->vcube);
    for (int i = 0; i < s->eventsLength; i++)
    {
        _cleanSimulationEvent(&s->events[i]);
    }
    deleteLog(s->log);
    free(s->events);
    free(s);
}

/**
 * @brief cria uma estrutura Simulation
 *
 * @param argv              argv
 * @param argc              argc
 * @param n                 numero de nodos
 * @return Simulation*      estrutura inicializada
 */
Simulation *createSimulation(char *argv[], int argc, long n)
{
    Simulation *s = (Simulation *)malloc(sizeof(Simulation));
    if (s == NULL) ERROR("Nao foi possivel alocar espaco para uma estrutura 'Simulation';\n");

    s->lastEvent = NULL;
    s->n = n;
    s->log = getSimulationLog(argv, argc);
    s->vcube = createVCube(n, s->log);

    s->events = (SimulationEvent *)malloc(INITIAL_SIMULATION_EVENTS * sizeof(SimulationEvent));
    if (!s->events) ERROR("Nao foi possivel alocar espaco para o vetor de estruturas 'Simulation::events'.\n");

    s->smplEvents = 0;
    s->eventsLength = 0;
    s->eventsAllocated = INITIAL_SIMULATION_EVENTS;

    for (int i = 0; i < INITIAL_SIMULATION_EVENTS; i++)
    {
        _initSimulationEvent(&s->events[i]);
    }

    readEvents(s, stdin);

    return s;
}

/**
 * @brief imprime a simulacao nos logs
 *
 * @param s     simulacao
 */
void logSimulation(Simulation *s)
{
    logAll(s->log, "NODOS: %ld\n", s->n);
    const char *event[] = {
        "",
        "INTERVALO DE TESTES",
        "FALHA",
        "RECUPERACAO",
        "BROADCAST"
    };
    logAll(s->log, "EVENTOS:\n");

    for (int i = 0; i < s->eventsLength; i++)
    {
        logAll(s->log, "\t%s", event[s->events[i].type]);
        if (s->events[i].type == EVENT_TYPE_FAULT || s->events[i].type == EVENT_TYPE_RECOVER || s->events[i].type == EVENT_TYPE_BROADCAST_SEND)
        {
            logAll(s->log, ": ");
            for (int j = 0; j < s->events[i].tokensLength; j++)
            {
                logAll(s->log, "%d ", s->events[i].tokens[j]);
            }
        }
        logAll(s->log, "\n");
    }
}

/**
 * @brief escalona um evento e conta
 *
 * @param s         simulacao
 * @param evt       evento
 * @param time      horario do evento
 * @param token     token do evento
 */
static void _scheduleEvent(Simulation *s, int evt, double time, int token)
{
    schedule(evt, time, token);
    s->smplEvents++;
}

/**
 * @brief escalona os eventos lidos do arquivo de simulacao
 *
 * @param s     simulacao
 */
static void _scheduleSimulationEvents(Simulation *s)
{
    double time = 0.0;
    for (int i = 0; i < s->eventsLength; i++)
    {
        for (int j = 0; j < s->events[i].tokensLength; j++)
        {
            _scheduleEvent(s, s->events[i].type, time, s->events[i].tokens[j]);
        }

        switch (s->events[i].type)
        {
            case EVENT_TYPE_TEST_INTERVAL:
                _scheduleEvent(s, EVENT_TYPE_CHECK_LATENCY, time + 1.0, -1);
                break;
            case EVENT_TYPE_FAULT:
            case EVENT_TYPE_RECOVER:
                _scheduleEvent(s, EVENT_TYPE_SAVE_EVENT, time + 1.0, i);
                _scheduleEvent(s, EVENT_TYPE_RESET_LATENCY, time + 1.0, -1);
                break;
            default: break;
        }

        time += 10.0;
    }
}

/**
 * @brief verifica se o evento "combina" com o STATE que o nodo [node] tem
 *        usado para verificar se o nodo [node] descobriu o evento.
 *
 * @param evt       evento
 * @param node      nodo alvo
 * @return int      1 se sim, 0 caso contrario
 */
static int _eventMatchesState(SimulationEvent *evt, VCubeNode *node)
{
    for (int i = 0; i < evt->tokensLength; i++)
    {
        if (evt->tokens[i] == node->token) continue;
        if (
            (evt->type == EVENT_TYPE_FAULT && IS_STATE_FAULT_FREE(node->STATE[evt->tokens[i]]))
            || (evt->type == EVENT_TYPE_RECOVER && IS_STATE_FAULTY(node->STATE[evt->tokens[i]]))
        )
        {
            return 0;
        }
    }
    return 1;
}

/**
 * @brief verifica se o ultimo evento foi descoberto
 * 
 * @param s         simulacao
 * @return int      1 se sim, 0 caso contrario
 */
static int _lastEventDiscovered(Simulation *s)
{
    if (s->lastEvent != NULL)
    {
        for (int i = 0; i < s->n; i++)
        {
            VCubeNode *node = &s->vcube->nodes[i];
            if (isTrulyFaulty(node)) continue;
            for (int j = 0; j < s->n; j++)
            {
                if (i == j) continue;
                if (!_eventMatchesState(s->lastEvent, node))
                {
                    return 0;
                }
            }
        }
    }
    else
    {
        for (int i = 0; i < s->n; i++)
        {
            VCubeNode *node = &s->vcube->nodes[i];
            if (isTrulyFaulty(node)) continue;
            for (int j = 0; j < s->n; j++)
            {
                if (isUnknown(node, j))
                {
                    return 0;
                }
            }
        }
    }
    return 1;
}

/**
 * @brief executa a simulacao
 *
 * @param s 
 */
void runSimulation(Simulation *s)
{
    int evt, token;
    int latency = 0;
    int lastEventDiscoveredMessageLogged = 0;

    _scheduleSimulationEvents(s);

    for (int i = 0; i < s->smplEvents; i++)
    {
        cause(&evt, &token);

        switch (evt)
        {
            case EVENT_TYPE_FAULT:
                _fail(s, token);
                break;
            case EVENT_TYPE_RECOVER:
                _recover(s, token);
                break;
            case EVENT_TYPE_TEST_INTERVAL:
                runTests(s->vcube, token);
                break;
            case EVENT_TYPE_BROADCAST_SEND:
                _broadcast(s, token);
                break;
            case EVENT_TYPE_BROADCAST_RECEIVE:
                _receive(s, token);
                break;
            case EVENT_TYPE_CHECK_LATENCY:
                latency++;
                if (!lastEventDiscoveredMessageLogged && _lastEventDiscovered(s))
                {
                    lastEventDiscoveredMessageLogged = 1;
                    if (s->lastEvent != NULL)
                    {
                        logDiagnostic(s->log, "ultimo evento descoberto (latencia: %d)\n", latency);
                    }
                    else
                    {
                        logDiagnostic(s->log, "estado de todos os nodos diagnosticado (latencia: %d)\n", latency);
                    }
                }
                break;
            case EVENT_TYPE_RESET_LATENCY:
                latency = 0;
                break;
            case EVENT_TYPE_SAVE_EVENT:
                s->lastEvent = &s->events[token];
                lastEventDiscoveredMessageLogged = 0;
                break;
            default: break;
        }
    }
}

/**
 * @brief "criptografa" origem [sender], [cluster] e destino [target] como um int
 *
 * @param sender    origem
 * @param cluster   cluster
 * @param target    destino
 * @param n         numero de nodos
 * @return int      mensagem "criptografada"
 */
static int _encrypt(int sender, int cluster, int target, int n)
{
    return (sender * n * n) + (cluster * n) + target;
}

/**
 * @brief inverso de _encrypt()
 *
 * @param message   mensagem "criptografada"
 * @param n         numero de nodos
 * @param sender    referencia para salvar origem
 * @param cluster   referencia para salvar cluster
 * @param target    referencia para salvar destino
 */
static void _decrypt(int message, int n, int *sender, int *cluster, int *target)
{
    *sender = message / (n * n);
    message = message % (n * n);

    *cluster = message / n;
    *target = message % n;
}

/**
 * @brief nodo [token] realiza broadcast.
 *        eventos de receive sao escalonados aqui.
 *
 * @param s         simulacao
 * @param token     nodo alvo
 */
static void _broadcast(Simulation *s, int token)
{
    if (isTrulyFaulty(&s->vcube->nodes[token])) return;
    int *targets = (int *)malloc(s->n * sizeof(int));
    getFirstFaultFreeNodeOfEachCluster(s->vcube, token, s->vcube->logN, targets);

    logMessage(s->log, "[%4.1f] [BROADCAST] %d: SEND(", time(), token);

    for (int i = 0; i < s->vcube->logN; i++)
    {
        if (targets[i] == -1) continue;
        int cluster, target;

        decryptClusterAndToken(targets[i], s->n, &cluster, &target);

        logMessage(s->log, "%d,", target);
        _scheduleEvent(s, EVENT_TYPE_BROADCAST_RECEIVE, 1, _encrypt(token, cluster, target, s->n));
    }

    logMessage(s->log, ")\n");

    free(targets);
}

/**
 * @brief nodo [target] (criptografado) recebe mensagem e
 *        a propaga se necessario
 *
 * @param s         simulacao
 * @param message   mensagem "criptografada"
 */
static void _receive(Simulation *s, int message)
{
    int sender, cluster, target;

    _decrypt(message, s->n, &sender, &cluster, &target);

    int self = target;

    if (isTrulyFaulty(&s->vcube->nodes[target])) return;

    logMessage(s->log, "[%4.1f] [RECEIVE] %d:(src=%d,s=%d) ", time(), target, sender, cluster);

    if (cluster == 1)
    {
        logMessage(s->log, "\n");
        return;
    }

    int *targets = (int *)malloc(s->n * sizeof(int));
    getFirstFaultFreeNodeOfEachCluster(s->vcube, target, cluster - 1, targets);

    logMessage(s->log, "PROPAGATE(");
    for (int i = 0; i < cluster - 1; i++)
    {
        if (targets[i] == -1) continue;
        int targetCluster;
        decryptClusterAndToken(targets[i], s->n, &targetCluster, &target);
        logMessage(s->log, "%d,", target);
        _scheduleEvent(s, EVENT_TYPE_BROADCAST_RECEIVE, 1, _encrypt(self, targetCluster, target, s->n));
    }
    logMessage(s->log, ")\n");

    free(targets);
}

/**
 * @brief falha o nodo com token dado
 *
 * @param s         simulacao-alvo
 * @param token     token-alvo
 */
static void _fail(Simulation *s, int token)
{
    if (token < 0 || token >= s->n) ERROR("ERRO: fail(s <%p>, token <%d>): token invalido.\n", s, token);
    if (isTrulyFaulty(&s->vcube->nodes[token])) ERROR("ERRO: Tentativa de falhar um nodo falho. (token: %d)\n", token);

    int r = request(s->vcube->nodes[token].id, token, 0);
    if (r != 0) ERROR("Erro em fail(s <%p>, token <%d>): Nao foi possivel falhar o nodo %d\n", s, token, token);

    logAll(s->log, "[%4.1f] [FALHA] %d\n", time(), token);
}

/**
 * @brief recupera o nodo com o token dado
 *
 * @param s         simulacao-alvo
 * @param token     token-alvo
 */
static void _recover(Simulation *s, int token)
{
    if (token < 0 || token >= s->n) ERROR("Erro em recover(s <%p>, token <%d>): token invalido.\n", s, token);
    if (isTrulyFaultFree(&s->vcube->nodes[token])) ERROR("ERRO: Tentativa de recuperar um nodo sem-falha. (token: %d)\n", token);

    release(s->vcube->nodes[token].id, token);

    for (long i = 0; i < s->n; i++)
        s->vcube->nodes[token].STATE[i] = NODE_STATE_UNKNOWN;
    s->vcube->nodes[token].STATE[token] = NODE_STATE_INITIAL_FAULT_FREE;

    logAll(s->log, "[%4.1f] [RECUPERACAO] %d\n", time(), token);
}
