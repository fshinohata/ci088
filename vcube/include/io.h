#ifndef __IO_MANAGER__
#define __IO_MANAGER__

char *getOptionValue(const char *option, char *argv[], int argc);
int hasOption(const char *option, char *argv[], int argc);
int isWord(char *w, char comment);
int isNumber(char *n, char comment);
int getNextValidLine(FILE *in, char *buffer, char comment);

#endif