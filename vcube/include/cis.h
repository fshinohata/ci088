#ifndef __CIS__
#define __CIS__

typedef struct NodeSet NodeSet;

struct NodeSet
{
	int* nodes;
	size_t size;
	size_t offset;
};

void deleteNodeSet(NodeSet *nodes);
NodeSet *cis(int i, int s);

#endif
